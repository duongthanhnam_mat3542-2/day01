function searchItems() {
    var keyword = $("#key_word").val();
    var facility = $("#select_facility").val();

    $.post("search.php", { key_word: keyword, facility: facility })
        .done(displayItems)
        .fail(function(error) {
            console.error("Error fetching data:", error);
        });
}

function displayItems(data) {
    var jsonData = JSON.parse(data);
    console.log(jsonData);

    var html = jsonData.map(function(student, index) {
        return `
            <tr>
                <td>${index + 1}</td>
                <td>${student.FULL_NAME}</td>
                <td>${student.FACILITY}</td>
                <td class='text-end'><button class='btn btn-danger'>Xóa</button></td>
                <td class='text-end'><button class='btn btn-info'>Sửa</button></td>
            </tr>
        `;
    }).join("");

    $("#total").html(jsonData.length);
    $(".list-student").html(html);
}
