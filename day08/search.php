<?php
    include '../day06/database.php';

    mysqli_query($conn, "USE ltweb");

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $facility = $_POST["facility"];
        $key_word = $_POST["key_word"];

        $facilityMappings = [
            "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu",
        ];

        $facility = $facilityMappings[$facility] ?? $facility;

        $sql = "SELECT * FROM `students` WHERE (FULL_NAME LIKE '%$key_word%')";
        $sql .= ($facility != "all") ? " AND FACILITY='$facility'" : "";

        $result = mysqli_query($conn, $sql);

        if ($result) {
            $listStudent = [];
            while ($row = mysqli_fetch_assoc($result)) {
                $listStudent[] = $row;
            }
            echo json_encode($listStudent, JSON_UNESCAPED_UNICODE);
            mysqli_free_result($result);
        } else {
            echo "The query failed: " . mysqli_error($conn);
        }
    }

    mysqli_close($conn);
?>
