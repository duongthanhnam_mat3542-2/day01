$(document).ready(function() {
    searchItems();
        $("#reset").on("click", () => {
            $("#key_word").val("");
            $("#select_facility").val("all");
            searchItems();
        });
        $("#key_word").on("keyup", () => {
            searchItems();
        });
        $("#select_facility").on("change", () => {
            searchItems();
        });
    });