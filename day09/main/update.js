$(document).ready(function() {
    displayEditItem($("#id").val());
    $('[type="submit"]').on('click', () => {
        validateMessage = "";
        let name = $('#full_name').val();
        let gender = $('[name="sex"]:checked').val();
        let facility = $('#facility').val();
        let date = $('#birthday').val();
        let address = $('#address').val();
        if (name == "") {
            validateMessage += "Hãy nhập tên. <br>";
        }
        if (gender == undefined) {
            validateMessage += "Hãy chọn giới tính. <br>";
        }
        if (facility == "") {
            validateMessage += "Hãy chọn phân khoa. <br>";
        }
        if (date == "") {
            validateMessage += "Hãy chọn ngày sinh. <br>";
        } else if ((date.slice(0, date.indexOf('-'))) > new Date().getFullYear()) {
            validateMessage += "Hãy nhập ngày sinh đúng định dạng. <br>";
        }
        $('.validate').html(validateMessage);
        if (validateMessage == "") {
            updateItem();
        }
    });
});