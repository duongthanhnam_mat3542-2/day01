<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
            padding: 20px;
        }
        label.col-sm-3 {
            background-color: #5b9bd5;
            border: solid #41719C;
            color: white;
        }
        .form-check-inline {
            margin-top: 9px;
        }
        .btn {
            background-color: #70ad47;
        }
        .btn-border {
            border: 3px solid #41719c;
        }
        .btn-back {
            background-color: #5b9bd5;
            border: 2px solid #41719c;
        }
       
    </style>
</head>
<body>
    <div class="container">
        <?php
            $name = $sex = $facility = "";
        ?>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
            <div class="mb-3 row">
                <label for="name" class="col-sm-3 col-form-label">Họ và tên</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $name; ?>">
                </div>
            </div>

            <div class="mb-3 row">
                <label for="sex" class="col-sm-3 col-form-label">Giới tính</label>
                <div class="col-sm-9">
                    <?php
                    $sex_arr = ["Nam", "Nữ"];
                    foreach ($sex_arr as $value):
                    ?>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input btn-back" type="radio" name="sex" id="sex_<?php echo strtolower($value); 
                            ?>" value="<?php echo $value; ?>" <?php if ($sex === $value) echo "checked"; ?>>
                        <label class="form-check-label" for="sex_<?php echo strtolower($value); ?>"><?php echo $value; 
                            ?></label>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="facility" class="col-sm-3 col-form-label">Phân khoa</label>
                <div class="col-sm-9">
                    <?php
                    $facility_arr = [
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    ];
                    ?>
                    <select name="facility" class="form-select" id="facility">
                        <option value="" selected>--Chọn phân khoa--</option>
                        <?php
                        foreach ($facility_arr as $key => $value):
                        ?>
                        <option value="<?php echo $key; ?>" <?php if ($facility === $key) echo "selected"; ?>><?php echo $value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-border btn-success mb-3 pe-5 ps-5">Đăng ký</button>
            </div>
        </form>
    </div>
</body>
</html>
