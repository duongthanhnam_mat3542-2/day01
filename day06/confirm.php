<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Submit Form</title>
</head>

<body>
    <?php
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') 
    {
        die;
    }

    if (!isset($_FILES["fileInput"])) 
    {
        die;
    }

    if ($_FILES["fileInput"]['error'] != 0)
    {
        
        die;
    }
    // $imagePath = $_FILES["fileInput"]["tmp_name"];
    // $imageFile = 'uploads/'. $_FILES["fileInput"]["name"];

    // move_uploaded_file($imagePath, $imageFile);
    ?>

<div class="container">
        <?php
        include 'database.php';
        
        mysqli_query($con, "ltweb");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["name"];
            $gender = ($_POST["sex"] == 0) ? "Nam" : "Nữ";
            $facility = ($_POST["facility"] == "MAT") ? "Khoa học máy tính" : "Khoa học vật liệu";
            $date = $_POST["date"];
            $address = $_POST["address"];

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["submitButton"])){
        $sql = "INSERT INTO `students` (`NAME`, `SEX`, `FALCUTY`, `BIRTH`, `ADDRESS`, `IMAGE`) 
        VALUES ('$name', '$gender', '$facility', '$date', '$address', '$imageFile')";
        }
        ?>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Họ và tên</span>
            <div class='col-sm-5'><?= $name ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Giới tính</span>
            <div class='col-sm-5'><?= $gender ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Phân khoa</span>
            <div class='col-sm-5'><?= $facility ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Ngày sinh</span>
            <div class='col-sm-5'><?= $date ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Địa chỉ</span>
            <div class='col-sm-5'><?= $address ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label' style='height:30%'>Hình ảnh</span>
            <div class='col-sm-5'>
                <img src='<?= $imageFile ?>' alt='Uploaded Image' class='img-fluid'>
            </div>
        </div>

        <div class='text-center'>
            <button name="submitButton" class='submit btn btn-success mb-3 pe-5 ps-5 mt-3'>Xác nhận</button>
        </div>
        <?php
        } else {
            echo "<p>Không có dữ liệu để hiển thị.</p>";
        }
        ?>
    </div>

</body>

</html>