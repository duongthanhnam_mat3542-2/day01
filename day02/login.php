<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login| Demo Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="style.css" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <?php
       
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        
        $datetime = date('d/m/Y');
        $hour = date('H') . " giờ "; 
        $minute = date('i') . " phút ";
        $day = date('w');
        $daysOfWeek = ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'];

        while ($daysOfWeek == 7) {
            $daysOfWeek = 1; // Reset to Monday
        }
        
        $day = $daysOfWeek[$day];
    ?>

    <div class="container-fuild">
        <div class="row">
            <div class="offset-md-3 col-md-6 text-center">
                <div class="pt-2 pb-2" style = "background-color: silver">Bây giờ là: 
                    <?php 
                        echo $hour . $minute . ", " . $day . " ngày " . $datetime;  
                    ?></div>
            </div>
        </div>
        <div class="row">
            <form action="Login.php" method="post" class="col-md-6 offset-md-3">
                <div class="mt-3" >
                    <label for="username" class="col-5 bg-info p-1" >Tên đăng nhập</label>
                    <input type="text" id="username" name="username" class="col-6 p-1 float-end" required>
                </div>

                <div class="mt-3">
                    <label for="password" class="col-5 bg-info p-1">Mật khẩu</label>
                    <input type="password" id="password" name="password" class="col-6 p-1 float-end" required>
                </div>

                <div class="mt-3 col-11 text-center">
                    <input type="submit" class="btn btn-info" value="Đăng nhập">
                </div>
            </form>
        </div>
    </div>
</body>

</html>
