<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Update Form</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="style.css">

    <script>
        $(document).ready(function() {
            $('[type="submit"]').on('click', function(event) {
                event.preventDefault(); 

                const fieldsToValidate = [
                    { id: 'name', message: 'Hãy nhập tên.' },
                    { id: 'sex', type: 'radio', message: 'Hãy chọn giới tính.' },
                    { id: 'facility', message: 'Hãy chọn phân khoa.' },
                    { id: 'date', message: 'Hãy chọn ngày sinh.' }
                ];
                
                let validateMessage = "";

                fieldsToValidate.forEach(field => {
                    const fieldValue = getFieldInputValue(field);
                    if (!fieldValue) {
                        validateMessage += `${field.message} <br>`;
                    } else if (field.id === 'date' && fieldValue.split('-')[0].length > 4) {
                        validateMessage += "Hãy nhập ngày sinh đúng định dạng. <br>";
                    }
                });

                $('.validate').html(validateMessage);

                if (!validateMessage) {
                    $('form').submit(); 
                }
            });

         
            function getFieldInputValue(field) {
                const fieldId = field.id;
                const fieldType = field.type || 'text';

                if (fieldType === 'radio') {
                    return $(`[name="${fieldId}"]:checked`).val();
                }

                return $(`#${fieldId}`).val();
            }
        });

    </script>
</head>

<body>
    <?php
        $name = $sex = $facility = "";
    ?>
    <div class="container p-5">
        <div class="validate">
        </div>
        <form action="#">
            <div class="mb-3 row">
                <label for="name" class="col-sm-2 col-form-label">Họ và tên<span class="require">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="" class="col-sm-2 col-form-label">Giới tính<span class="require">*</span></label>
                <div class="col-sm-10">
                <?php
                    $sex_arr = ["Nam", "Nữ"];
                    foreach ($sex_arr as $value):
                    ?>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input btn-back" type="radio" name="sex" id="sex_<?php echo strtolower($value); 
                            ?>" value="<?php echo $value; ?>" <?php if ($sex === $value) echo "checked"; ?>>
                        <label class="form-check-label" for="sex_<?php echo strtolower($value); ?>">
                        <?php echo $value; ?></label>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="select_facility" class="col-sm-2 col-form-label ">Phân khoa<span class="require">*</span></label>
                <div class="col-sm-5">
                <?php
                    $facility_arr = [
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    ];
                    ?>
                    <select name="facility" class="form-select" id="facility">
                        <option value="" selected>--Chọn phân khoa--</option>
                        <?php
                        foreach ($facility_arr as $key => $value):
                        ?>
                        <option value="<?php echo $key; ?>" <?php if ($facility === $key) echo "selected"; ?>><?php echo $value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            
            <div class="mb-3 row">
                <label for="date" class="col-sm-2 col-form-label ">Ngày sinh<span class="require">*</span></label>
                <div class="col-sm-5">
                    <input type="date" class="form-control" id="date" name="date">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="name" class="col-sm-2 col-form-label" style="height: 30%;">Địa chỉ</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3"></textarea>
                </div>
            </div>
        </form>
        <div class="text-center">
            <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Đăng ký</button>
        </div>
    </div>

    <div class="container p-5">
        <div class="text-center">
            <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Đăng ký</button>
        </div>
    </div>
</body>
</html>
