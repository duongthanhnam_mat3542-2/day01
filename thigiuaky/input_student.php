<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Update Form</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="style.css">

    <script>
       $(document).ready(function() {
        $('[type="submit"]').on('click', function(event) {
            event.preventDefault();

            const fieldsToValidate = [
                { id: 'name', message: 'Hãy nhập tên.' },
                { id: 'sex', type: 'radio', message: 'Hãy chọn giới tính.' },
                { id: 'facility_year', message: 'Hãy chọn năm sinh.' },
                { id: 'facility_month', message: 'Hãy chọn tháng sinh.' },
                { id: 'facility_day', message: 'Hãy chọn ngày sinh.' },
                { id: 'city', message: 'Hãy chọn thành phố.' },
                { id: 'district', message: 'Hãy chọn quận.' },
            ];

            let validateMessage = "";

            fieldsToValidate.forEach(field => {
                const fieldValue = getFieldInputValue(field);
                if (fieldValue === null || fieldValue === "") {
                    validateMessage += `${field.message} <br>`;
                } else if (
                    (field.id === 'facility_year' || field.id === 'facility_month' || field.id === 'facility_day') &&
                    isNaN(fieldValue)
                ) {
                    validateMessage += `${field.message} <br>`;
                }
            });

            $('.validate').html(validateMessage);

            if (!validateMessage) {
                $('form').submit();
            }
        });

        function getFieldInputValue(field) {
            const fieldId = field.id;
            const fieldType = field.type || 'text';

            if (fieldType === 'radio') {
                return $(`[name="${fieldId}"]:checked`).val();
            }

            return $(`#${fieldId}`).val();
        }
    });

    </script>
</head>

<body>
    <?php
        $name = $sex = $facility = "";
    ?>
    <div class="container p-5">
        <div class="text-center">
            <h4>
                Form đăng ký sinh viên
            </h4>
        </div>
        <div class="validate">
        </div>
        <form action="regist_student.php" method="post" enctype="multipart/form-data">
            <div class="mb-3 row">
                <label for="name" class="col-sm-2 col-form-label">Họ và tên<span class="require">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="" class="col-sm-2 col-form-label">Giới tính<span class="require">*</span></label>
                <div class="col-sm-10">
                <?php
                    $sex_arr = ["Nam", "Nữ"];
                    foreach ($sex_arr as $value):
                    ?>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input btn-back" type="radio" name="sex" id="sex_<?php echo strtolower($value); 
                            ?>" value="<?php echo $value; ?>" <?php if ($sex === $value) echo "checked"; ?>>
                        <label class="form-check-label" for="sex_<?php echo strtolower($value); ?>">
                        <?php echo $value; ?></label>
                    </div>
                    <?php endforeach; ?>    
                </div>
            </div>

            <div class="mb-3 row">
                <label for="select_facility" class="col-sm-2 col-form-label">Ngày sinh<span class="require">*</span></label>
                <div class="col-sm-3">
                    <select name="facility_year" class="form-select" id="facility_year">
                        <option value="" selected>--Năm--</option>
                        <?php
                        $current_year = date("Y");

                        $start_year = $current_year - 40;
                        $end_year = $current_year + 15;

                        $facility_arr = range($end_year, $start_year);

                        $facility_arr = array_reverse($facility_arr);

                        foreach ($facility_arr as $year):
                            ?>
                            <option value="<?php echo $year; ?>" <?php if ($facility === $year) echo "selected"; ?>><?php echo $year; ?></option>
                        <?php endforeach;
                        ?>
                    </select>
                </div>

                <div class="col-sm-3">
                    <?php
                    $facility_arr = range(1,12);
                    ?>
                    <select name="facility_month" class="form-select" id="facility_month">
                        <option value="" selected>--Tháng--</option>
                        <?php
                        foreach ($facility_arr as $key):
                            ?>
                            <option value="<?php echo $key; ?>" <?php if ($facility === $key) echo "selected"; ?>><?php echo $key; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-sm-3">
                    <?php
                    $facility_arr = range(1,31);
                    ?>
                    <select name="facility_day" class="form-select" id="facility_day">
                        <option value="" selected>--Ngày--</option>
                        <?php
                        foreach ($facility_arr as $key):
                            ?>
                            <option value="<?php echo $key; ?>" <?php if ($facility === $key) echo "selected"; ?>><?php echo $key; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>




            <div class="mb-3 row">
                <label for="select_city" class="col-sm-2 col-form-label">Địa chỉ<span class="require">*</span></label>
                <div class="col-sm-5">
                    <select name="city" class="form-select" id="city">
                        <option value="" selected>--Thành phố--</option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="Tp.Hồ Chí Minh">Tp.Hồ Chí Minh</option>
                    </select>
                </div>
                <div class="col-sm-5">
                    <select name="district" class="form-select" id="district">
                        <option value="" selected>--Quận--</option>
                    </select>
                </div>
            </div>

            <script>
                const citySelect = document.getElementById('city');
                const districtSelect = document.getElementById('district');

                const districtOptions = {
                    "Hà Nội": [
                        "Hoàng Mai",
                        "Thanh Trì",
                        "Nam Từ Liêm",
                        "Hà Đông",
                        "Cầu Giấy"
                    ],
                    "Tp.Hồ Chí Minh": [
                        "Quận 1",
                        "Quận 2",
                        "Quận 3",
                        "Quận 7",
                        "Quận 9"
                    ],
                };

                function updateDistrictOptions() {
                    const selectedCity = citySelect.value;
                    const districts = districtOptions[selectedCity] || [];

                    districtSelect.innerHTML = '';

                    districts.forEach(district => {
                        const option = document.createElement('option');
                        option.value = district;
                        option.text = district;
                        districtSelect.appendChild(option);
                    });
                }

                citySelect.addEventListener('change', updateDistrictOptions);

                updateDistrictOptions();
            </script>

            <div class="mb-3 row">
                <label for="other-info" class="col-sm-2 col-form-label">Thông tin khác</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="other-info" name="other-info" rows="4"></textarea>
                </div>
            </div>

        </form>
        <div class="text-center">
            <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Đăng ký</button>
        </div>
    </div>
</body>
</html>
