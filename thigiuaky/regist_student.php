<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin đăng ký</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>

<body>
<div class="container p-5">
        <div class="text-center">
            <h4>
                Thông tin sinh viên đăng ký 
            </h4>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Họ và tên</span>
            <div class='col-sm-5'> <?php echo $_POST['name']; ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Giới tính</span>
            <div class='col-sm-5'><?php echo $_POST['sex']; ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Ngày sinh</span>
            <div class='col-sm-5'><?php
                $facility_year = $_POST['facility_year'];
                $facility_month = $_POST['facility_month'];
                $facility_day = $_POST['facility_day'];
                echo "$facility_day/$facility_month/$facility_year";
                ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Địa chỉ</span>
            <div class='col-sm-5'><?php
                $city = $_POST['city'];
                $district = $_POST['district'];
                echo "$district, $city";
                ?></div>
        </div>

        <div class='mb-3 row'>
            <span class='col-sm-2 col-form-label'>Thông tin khác</span>
            <div class='col-sm-5'><?php echo $_POST['other-info']; ?></div>
        </div>
        
    </div>
</body>

</html>
