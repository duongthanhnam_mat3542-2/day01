<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <?php
    // Include the database connection and query functions in a separate file.
    include 'database.php';
    
    // Initialize variables for filtering and searching.
    $facility_filter = "all";
    $key_word = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $facility_filter = $_POST["facility"];
        $key_word = $_POST["key_word"];
    }

    // Fetch students based on filtering and searching criteria.
    $students = getStudents($facility_filter, $key_word);

    $count = count($students);
    ?>

    <div class="container filter">
        <form action="" method="POST">
            <div class="mb-3 row">
                <label for="select_facility" class="offset-sm-2 col-sm-2 col-form-label">Khoa</label>
                <div class="col-sm-5">
                    <select name="facility" class="form-select" id="select_facility">
                        <option value="all" <?= ($facility_filter == "all") ? 'selected' : '' ?>>Tất cả</option>
                        <option value="MAT" <?= ($facility_filter == "Khoa học máy tính") ? 'selected' : '' ?>>Khoa học máy tính</option>
                        <option value="KDL" <?= ($facility_filter == "Khoa học vật liệu") ? 'selected' : '' ?>>Khoa học vật liệu</option>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="key_word" class="offset-sm-2 col-sm-2 col-form-label">Từ khóa</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="key_word" name="key_word" value="<?= $key_word ?>">
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Tìm kiếm</button>
            </div>
        </form>
    </div>

    <div class="container list-student">
        <div class="mb-3 row">
            <div class="col-sm-3">
                Số sinh viên tìm thấy: <?= $count ?>
            </div>
            <div class="offset-sm-8 col-sm-1">
                <a href="register.php" class="btn btn-success">Thêm</a>
            </div>
        </div>
        <div class="mb-3 row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tên sinh viên</th>
                        <th scope="col">Khoa</th>
                        <th scope="col" class="text-end">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($count > 0) { ?>
                        <?php foreach ($students as $index => $student) { ?>
                            <tr>
                                <th scope="row"><?= $index + 1 ?></th>
                                <td><?= $student["FULL_NAME"] ?></td>
                                <td><?= $student["FACILITY"] ?></td>
                                <td class="text-end">
                                    <a class="btn btn-primary ms-3">Xóa</a>
                                    <a class="btn btn-danger ms-3">Sửa</a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="4" class="text-center">Không có sinh viên nào</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
